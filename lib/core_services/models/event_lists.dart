import 'event.dart';

class EventList {
  List<Event> events;

  EventList({
    this.events,
  });

  factory EventList.fromJson(List<dynamic> parsedJson) {
    List<Event> events = new List<Event>();
    events = parsedJson.map((i) => Event.fromJson(i)).toList();
    print('EVENT LIST JSON');
    return EventList(
      events: events,
    );
  }
}
