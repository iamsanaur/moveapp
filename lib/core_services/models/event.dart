class Event {
  int id;
  String eventName;
  String eventDesc;
  String eventDp;
  EventLocation eventLocation;
  List<String> itemsList;
  String eventCategory;
  int eventCategoryColor;
  List<int> eventPeopleTarget;
  int eventPeopleGoing;
  String eventAddress;
  String eventTimeStamp;
  List<String> eventSpecialRequests;

  Event(
      {this.id,
      this.eventName,
      this.eventDesc,
      this.eventDp,
      this.eventLocation,
      this.itemsList,
      this.eventCategory,
      this.eventCategoryColor,
      this.eventPeopleTarget,
      this.eventPeopleGoing,
      this.eventAddress,
      this.eventTimeStamp,
      this.eventSpecialRequests});

  Event.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    eventName = json['event_name'];
    eventDesc = json['event_desc'];
    eventDp = json['event_dp'];
    eventLocation = json['event_location'] != null
        ? new EventLocation.fromJson(json['event_location'])
        : null;
    itemsList = json['items_list'].cast<String>();
    eventCategory = json['event_category'];
    eventCategoryColor = json['event_category_color'];
    eventPeopleTarget = json['event_people_target'].cast<int>();
    eventPeopleGoing = json['event_people_going'];
    eventAddress = json['event_address'];
    eventTimeStamp = json['event_time_stamp'];
    eventSpecialRequests = json['event_special_requests'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['event_name'] = this.eventName;
    data['event_desc'] = this.eventDesc;
    data['event_dp'] = this.eventDp;
    if (this.eventLocation != null) {
      data['event_location'] = this.eventLocation.toJson();
    }
    data['items_list'] = this.itemsList;
    data['event_category'] = this.eventCategory;
    data['event_category_color'] = this.eventCategoryColor;
    data['event_people_target'] = this.eventPeopleTarget;
    data['event_people_going'] = this.eventPeopleGoing;
    data['event_address'] = this.eventAddress;
    data['event_time_stamp'] = this.eventTimeStamp;
    data['event_special_requests'] = this.eventSpecialRequests;
    return data;
  }
}

class EventLocation {
  String type;
  List<double> coordinates;

  EventLocation({this.type, this.coordinates});

  EventLocation.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    coordinates = json['coordinates'].cast<double>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['type'] = this.type;
    data['coordinates'] = this.coordinates;
    return data;
  }
}
