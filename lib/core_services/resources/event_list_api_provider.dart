import 'dart:async';
import 'package:http/http.dart' show Client;
import 'dart:convert';
import '../models/event_lists.dart';

class EventApiProvider {
  Client client = Client();

  Future<EventList> fetchEventList() async {
    print("entered");
    final response = await client
        .get("http://192.168.1.156:3000/postgres/public/event_lists");
    print(response.body.toString());
    if (response.statusCode == 200) {
      print('RESPONSE RECIEVED');
      return EventList.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load post');
    }
  }
}
