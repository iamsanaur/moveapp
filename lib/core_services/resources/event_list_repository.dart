import 'dart:async';
import 'event_list_api_provider.dart';
import '../models/event_lists.dart';

class Repository {
  final eventApiProvider = EventApiProvider();

  Future<EventList> fetchEventList() => eventApiProvider.fetchEventList();
}
