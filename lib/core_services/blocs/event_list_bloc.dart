import '../resources/event_list_repository.dart';
import 'package:rxdart/rxdart.dart';
import '../models/event_lists.dart';

class EventListBloc {
  final _repository = Repository();
  final _eventFetcher = PublishSubject<EventList>();

  Observable<EventList> get allEvent => _eventFetcher.stream;

  fetchAllEvent() {
    getData();
    return _eventFetcher.stream;
  }

  getData() async {
    EventList itemModel = await _repository.fetchEventList();

    _eventFetcher.sink.add(itemModel);
  }

  dispose() {
    _eventFetcher.close();
  }
}

final bloc = EventListBloc();
