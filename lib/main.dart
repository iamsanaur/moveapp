import 'package:move_project/ui/account.dart';
import 'package:move_project/ui/my_event.dart';
import 'package:move_project/ui/nearby_events.dart';
import 'package:move_project/ui/settings.dart';
import 'package:flutter_svg/svg.dart';

import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Move App',
      theme: new ThemeData(
          primarySwatch: Colors.red, accentColor: Colors.redAccent),
      home: new MyHomePage(title: 'M-O-V-E'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentIndex = 0;
  final _pageOptions = [
    NearbyEvents(),
    MyEvent(),
    Settings(),
    AccountDetails()
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        foregroundColor: Colors.red.shade300,
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      backgroundColor: Colors.grey.shade100,
      appBar: AppBar(
          elevation: 0.5,
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SvgPicture.asset(
                'assets/logo.svg',
                fit: BoxFit.contain,
                height: 40.0,
                color: Colors.red.shade600,
              ),
            ],
          )),
      body: SafeArea(top: true, child: _pageOptions[_currentIndex]),
      bottomNavigationBar: BottomNavigationBar(
          elevation: 10,
          selectedFontSize: 15,
          iconSize: 30,
          selectedItemColor: Colors.redAccent,
          unselectedItemColor: Colors.grey.shade700,
          unselectedLabelStyle: TextStyle(color: Colors.grey),
          currentIndex: _currentIndex,
          onTap: (int index) {
            setState(() {
              _currentIndex = index;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.near_me),
                title: Text(
                  'Nearby Events',
                  style: TextStyle(fontFamily: 'Ubuntu'),
                )),
            BottomNavigationBarItem(
                icon: Icon(Icons.event),
                title:
                    Text('My Events', style: TextStyle(fontFamily: 'Ubuntu'))),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                title:
                    Text('Settings', style: TextStyle(fontFamily: 'Ubuntu'))),
            BottomNavigationBarItem(
                icon: Icon(Icons.account_circle),
                title: Text('Account', style: TextStyle(fontFamily: 'Ubuntu'))),
          ]),
    );
  }
}
