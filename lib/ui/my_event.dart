import 'package:flutter/material.dart';
import 'package:move_project/ui/components/top_bar.dart';

class MyEvent extends StatelessWidget {
  const MyEvent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        TopBar(
          title: "My Events",
          location: "Noida",
        ),
        Expanded(
          child: Container(),
        )
      ],
    );
  }
}
