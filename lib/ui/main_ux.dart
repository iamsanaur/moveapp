import 'package:flutter/material.dart';
import 'package:move_project/ui/settings.dart';
import 'package:move_project/ui/my_event.dart';
import 'package:move_project/ui/nearby_events.dart';

class MainUX extends StatefulWidget {
  @override
  _MainAppBarState createState() => _MainAppBarState();
}

class _MainAppBarState extends State<MainUX> {
  final List<Widget> _options = [NearbyEvents(), MyEvent(), Settings()];

  @override
  Widget build(BuildContext context) {
    int _selectedIndex = 0;

    void _onItemTapped(int index) {
      setState(() {
        _selectedIndex = index;
      });
    }

    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0.0,
          backgroundColor: Colors.white,
          title: Text(
            'M O V E',
            style: TextStyle(
                color: Colors.redAccent,
                fontFamily: 'Coolvetica',
                fontStyle: FontStyle.normal,
                fontSize: 45),
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.exit_to_app),
              iconSize: 22,
              color: Colors.redAccent,
              onPressed: () {},
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          elevation: 0,
          backgroundColor: Colors.redAccent,
          child: InkWell(
            child: Icon(Icons.add_comment),
          ),
          onPressed: () {},
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.local_activity),
              title: Text('Nearby'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.event),
              title: Text('My Events'),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              title: Text('Settings'),
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.redAccent,
          onTap: _onItemTapped,
        ),
        body: _options[_selectedIndex]);
  }
}
