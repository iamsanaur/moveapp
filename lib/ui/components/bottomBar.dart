import 'package:flutter/material.dart';

class BottomBar {
  const BottomBar(this.title, this.icon);
  final String title;
  final IconData icon;
}

const List<BottomBar> allOptions = <BottomBar>[
  BottomBar('Nearby Events', Icons.near_me),
  BottomBar('My Events', Icons.today),
  BottomBar('Settings', Icons.settings),
  BottomBar('Account', Icons.account_circle)
];
