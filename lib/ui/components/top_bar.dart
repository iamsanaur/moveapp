import 'package:flutter/material.dart';

class TopBar extends StatefulWidget {
  final String title;
  final String location;
  TopBar({this.title, this.location});
  @override
  _TopBarState createState() => _TopBarState();
}

class _TopBarState extends State<TopBar> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 85,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Colors.white, boxShadow: [
            BoxShadow(
                color: Colors.grey.shade300,
                offset: Offset.zero,
                spreadRadius: 2)
          ]),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
            child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    widget.title,
                    style: TextStyle(
                        fontSize: 26,
                        color: Colors.redAccent,
                        fontFamily: 'NotoSans'),
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.location_on,
                        size: 24,
                        color: Colors.redAccent,
                      ),
                      Text(
                        widget.location,
                        style: TextStyle(
                            color: Colors.redAccent,
                            fontSize: 16,
                            fontFamily: 'NotoSans'),
                      ),
                    ],
                  )
                ]),
          ),
        ),
      ],
    );
  }
}
