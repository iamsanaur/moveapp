import 'dart:math';
import 'package:flutter/material.dart';
import 'package:move_project/ui/components/top_bar.dart';
import 'package:move_project/core_services/blocs/event_list_bloc.dart';
import 'package:move_project/core_services/models/event_lists.dart';

class NearbyEvents extends StatefulWidget {
  @override
  _NearbyEventsState createState() => _NearbyEventsState();
}

class _NearbyEventsState extends State<NearbyEvents> {
  final List<Color> circleColors = [
    Colors.redAccent[50],
    Colors.orange,
    Colors.purple
  ];

  Color randomGenerator() {
    return circleColors[new Random().nextInt(3)];
  }

  bool isFav = false;
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<EventList>(
      stream: bloc.fetchAllEvent(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return buildEventList(snapshot);
        } else if (snapshot.hasError) {
          return AlertDialog(title: Text(snapshot.error.toString()));
        }
        return Center(child: CircularProgressIndicator());
      },
    );
  }

  Future<Null> _refreshEventList() async {
    return await bloc.fetchAllEvent();
  }

  Widget buildEventList(AsyncSnapshot<EventList> snapshot) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        TopBar(
          title: "Nearby Events",
          location: "Noida",
        ),
        Expanded(
          child: RefreshIndicator(
            onRefresh: _refreshEventList,
            child: ListView.builder(
                itemCount: snapshot.data.events.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: const EdgeInsets.only(
                        top: 3, left: 5, bottom: 3, right: 5),
                    child: Card(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 25.0, left: 10),
                            child: CircleAvatar(
                              radius: 35,
                              backgroundImage: NetworkImage(
                                  snapshot.data.events[index].eventDp),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 8),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  snapshot.data.events[index].eventName,
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Ubuntu'),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.local_post_office,
                                          color: Colors.redAccent,
                                        ),
                                        Text(
                                            ' ' +
                                                snapshot.data.events[index]
                                                    .eventAddress,
                                            style: TextStyle(
                                                fontSize: 13,
                                                fontFamily: 'Ubuntu')),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.timer,
                                          color: Colors.redAccent,
                                        ),
                                        Text(
                                            ' ' +
                                                snapshot.data.events[index]
                                                    .eventTimeStamp,
                                            style: TextStyle(
                                                fontSize: 13,
                                                fontFamily: 'Ubuntu')),
                                      ],
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Icon(
                                      Icons.group,
                                      color: Colors.redAccent,
                                    ),
                                    Text(
                                      '  ' +
                                          snapshot.data.events[index]
                                              .eventPeopleGoing
                                              .toString(),
                                      style: TextStyle(
                                          fontSize: 13, fontFamily: 'Ubuntu'),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Icon(
                                            Icons.favorite,
                                            color: Colors.redAccent,
                                          ),
                                          Text(
                                            ' ' +
                                                snapshot.data.events[index]
                                                    .eventPeopleGoing
                                                    .toString(),
                                            style: TextStyle(
                                                fontSize: 13,
                                                fontFamily: 'Ubuntu'),
                                          )
                                        ]),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Icon(
                                            Icons.category,
                                            color: Colors.redAccent,
                                          ),
                                        ]),
                                  ],
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 8,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              IconButton(
                                icon: Icon(
                                  Icons.favorite,
                                  color: isFav ? Colors.red : Colors.grey,
                                ),
                                onPressed: () {},
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.share,
                                  color: Colors.blue,
                                ),
                                onPressed: () {},
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.location_on,
                                  color: Colors.redAccent,
                                ),
                                onPressed: () {},
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                }),
          ),
        ),
      ],
    );
  }
}
