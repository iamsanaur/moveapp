import 'package:flutter/material.dart';
import 'package:move_project/ui/components/top_bar.dart';

class Settings extends StatelessWidget {
  const Settings({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TopBar(
      title: "Settings",
      location: "Noida",
    );
  }
}
